#ifndef IMAGE_TRANSFORMER_BMP_HANDLER_H
#define IMAGE_TRANSFORMER_BMP_HANDLER_H

#include "image.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


enum status_r {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,

};

enum status_r from_bmp(FILE *in, struct image **image);

enum status_w {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum status_w to_bmp(FILE *out, struct image const *image);


#endif
