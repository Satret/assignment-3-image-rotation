#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATOR_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATOR_H

struct image rotate_image(struct image src);

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATOR_H
