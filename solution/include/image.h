#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include  <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image *create_image_sample(uint64_t width, uint64_t height);

void image_free(struct image *image);
void image_data_free(struct image image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
