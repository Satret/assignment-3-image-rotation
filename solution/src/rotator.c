#include "rotator.h"
#include "image.h"
#include <stdlib.h>
struct image rotate_image(struct image src) {
    struct image out = {
            .height = src.width,
            .width = src.height,
            .data = malloc(sizeof(struct pixel) * src.height * src.width)
    };

    for (uint64_t i = 0; i < out.height; i++) {
        for (uint64_t column = 0; column < out.width; column++) {
            out.data[i * out.width + column] = src.data[(src.height - 1 - column) * src.width + i];
        }
    }

    return out;
}
