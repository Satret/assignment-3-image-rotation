#include "bmp.h"
#include "rotator.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Неправильное кол-во аргументов");
        return 1;
    }

    FILE *file_r = fopen(argv[1], "rb");
    FILE *file_w = fopen(argv[2], "wb");


    if (!file_r || !file_w) {
        fprintf(stderr, "Ошибка создания файла");
        return 1;
    }
    struct image *image_in;
    enum status_r status_r = from_bmp(file_r, &image_in);
    if (status_r) {
        fprintf(stderr, "Ошибка чтения: %d", status_r);
        return 1;
    }

    if (fclose(file_r)) {
        fprintf(stderr, "Ошибка закрытия входного файла");
        return 1;
    }

    struct image image_out = rotate_image(*image_in);
    image_free(image_in);

    enum status_w status_w = to_bmp(file_w, &image_out);
    if (status_w != WRITE_OK) {
        fprintf(stderr, "Ошибка записи файла: %d", status_w);
        return 1;
    }

    image_data_free(image_out);

    if (fclose(file_w)) {
        fprintf(stderr, "Ошибка закрытия выходного файла");
        return 1;
    }

    return 0;
}
