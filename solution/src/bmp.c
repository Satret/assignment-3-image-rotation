#include "../include/bmp.h"

#define LITTLE_ENDIAN 0x4d42
#define BYTE_OFFSET 54
#define SIZE 40
#define PLAIN 1
#define BIT_COUNT 24

inline static uint8_t size_of_padding(uint64_t width) {
if (width % 4) {
return 4 - (width * sizeof(struct pixel) % 4);
}
return 0;

}

inline static uint32_t image_size(struct image image) {
    return image.height * (sizeof(struct pixel) * image.width + size_of_padding(image.width));
}

inline static uint32_t file_size(struct image image) {
    return sizeof(struct bmp_header) + image_size(image);
}

enum status_r check_header(struct bmp_header header) {
    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    if (header.bfType != LITTLE_ENDIAN) {
        return READ_INVALID_SIGNATURE;
    }

    return READ_OK;
}

enum status_r from_bmp(FILE *in, struct image **image) {

    struct bmp_header header = {0};

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        printf("чтение недопустимого заголовка");
        return READ_INVALID_HEADER;
    }

    if (check_header(header) != READ_OK) {
        return check_header(header);
    }

    *image = create_image_sample(header.biWidth, header.biHeight);
    struct image *img = *image;

    size_t count = 0;
    for (size_t row = 0; row < header.biHeight; row++) {
        for (size_t column = 0; column < header.biWidth; column++) {
            if (!fread((img->data + count), sizeof(struct pixel), 1, in)) {
                image_free(img);
                return READ_INVALID_BITS;
            }
            count++;
        }
        if (fseek(in, size_of_padding(img->width), SEEK_CUR)) {
            image_free(img);
            return READ_INVALID_SIGNATURE;
        }
    }

    return READ_OK;
}

enum status_w to_bmp(FILE *out, struct image const *image) {
    struct bmp_header header = {
            .bfType = LITTLE_ENDIAN,
            .bfileSize = file_size(*image),
            .bfReserved = 0,
            .bOffBits = BYTE_OFFSET,
            .biSize = SIZE,
            .biWidth = (*image).width,
            .biHeight = (*image).height,
            .biPlanes = PLAIN,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = image_size(*image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out) || fseek(out, (uint32_t) header.bOffBits, SEEK_SET)) {
        return WRITE_ERROR;
    }

    struct pixel *pix = image->data;
    for (size_t i = 0; i < image->height; i++) {
        const size_t row_size = sizeof(struct pixel) * image->width;
        struct pixel *target_string = image->data + i * image->width;
        if (!fwrite(target_string, row_size, 1, out) ||
            (size_of_padding(image->width) != 0 && !fwrite(pix, size_of_padding(image->width), 1, out))) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
