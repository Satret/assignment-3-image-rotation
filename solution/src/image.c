#include "image.h"
#include <stdlib.h>

struct image *create_image_sample(uint64_t width, uint64_t height) {
    struct image *image = malloc(sizeof(struct image));
    struct pixel *data = malloc(sizeof(struct pixel) * width * height);
    image->width = width;
    image->height = height;
    image->data = data;
    return image;
}

void image_data_free(struct image image) {
    free(image.data);
}

void image_free(struct image *image) {
    image_data_free(*image);
    free(image);
}
